package com.kshrd.moviesservice.service.serviceImp;

import com.kshrd.moviesservice.model.Category;
import com.kshrd.moviesservice.payload.Request.CategoryRequest;
import com.kshrd.moviesservice.repository.CategoryRepository;
import com.kshrd.moviesservice.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImg implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category addCategory(CategoryRequest categoryRequest) {
        Category category= new Category();
        category.setName(categoryRequest.getName());
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findOne(Long id) {
        return categoryRepository.findCategoryById(id);
    }

    @Override
    public Category update(Long id, CategoryRequest categoryRequest) {
        Category category =  categoryRepository.findCategoryById(id);
        category.setName(categoryRequest.getName());
        return categoryRepository.save(category);
    }

    @Override
    public void delete(Long id) {
        Category category = categoryRepository.findCategoryById(id);
        categoryRepository.delete(category);
    }
}
