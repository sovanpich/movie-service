package com.kshrd.moviesservice.service.serviceImp;

import com.kshrd.moviesservice.model.Image;
import com.kshrd.moviesservice.payload.Request.ImageRequest;
import com.kshrd.moviesservice.repository.ImageRepository;
import com.kshrd.moviesservice.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ImageServiceImp implements ImageService {
    @Autowired
    private ImageRepository imageRepository;
    @Override
    public Image addImage(ImageRequest imageRequest) {
        Image image = new Image();
        image.setCover(imageRequest.getCover());
        image.setProfile(imageRequest.getProfile());
        return imageRepository.save(image);
    }

    @Override
    public List<Image> findAll() {
        return imageRepository.findAll();
    }

    @Override
    public Image findOne(Long id) {
        return imageRepository.findImageById(id);
    }

    @Override
    public Image update(Long id, ImageRequest imageRequest) {
        Image image = imageRepository.findImageById(id);
        image.setId(id);
        image.setCover(imageRequest.getCover());
        image.setProfile(imageRequest.getProfile());
        return imageRepository.save(image);
    }

    @Override
    public void delete(Long id) {
        Image image = imageRepository.findImageById(id);
        imageRepository.delete(image);
    }
}
