package com.kshrd.moviesservice.service;

import com.kshrd.moviesservice.model.Image;
import com.kshrd.moviesservice.payload.Request.ImageRequest;

import java.util.List;

public interface ImageService {

    Image addImage(ImageRequest imageRequest);

    List<Image> findAll();

    Image findOne(Long id);

    Image update(Long id, ImageRequest imageRequest);

    void delete(Long id);
}
