package com.kshrd.moviesservice.payload.Request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieRequest {
    private String title;
    private String description;
    private Integer release;
    private Integer rate;
    private int image_id;
    private int category_id;
}
