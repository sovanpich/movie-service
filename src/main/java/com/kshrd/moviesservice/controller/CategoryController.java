package com.kshrd.moviesservice.controller;

import com.kshrd.moviesservice.Response.RecordNotFoundException;
import com.kshrd.moviesservice.Response.ResponseHeader;
import com.kshrd.moviesservice.model.Category;
import com.kshrd.moviesservice.payload.Request.CategoryRequest;
import com.kshrd.moviesservice.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/add")
    public ResponseEntity<Object> save(@RequestBody CategoryRequest categoryRequest){
        try {
            Category category = categoryService.addCategory(categoryRequest);
            return ResponseHeader.generateResponse("Successful",HttpStatus.OK,category);
        }catch (Exception e){
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
    @GetMapping("/findAll")
    public ResponseEntity<Object> findAll(){
        try {
            List<Category> categoryList = categoryService.findAll();
            return ResponseHeader.generateResponse("Successful",HttpStatus.OK,categoryList);
        }catch (Exception e){
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
    @GetMapping("/findOne/{id}")
    public ResponseEntity<Object> findOne(@PathVariable Long id){
        Category category = categoryService.findOne(id);
        if (category==null){
            throw new RecordNotFoundException("Invalid Category Id" + id);
        }
        return ResponseHeader.generateResponse("Successful",HttpStatus.OK,category);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(@PathVariable Long id,@RequestBody CategoryRequest categoryRequest){
        try{
            Category category = categoryService.update(id, categoryRequest);
            return ResponseHeader.generateResponse("Successful",HttpStatus.OK,category);
        }catch (Exception e){
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id){
        try {
            categoryService.delete(id);
            return new ResponseEntity<>("Successful",HttpStatus.OK);
        }catch (Exception E){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

}
