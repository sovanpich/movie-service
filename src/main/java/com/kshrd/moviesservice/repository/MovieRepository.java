package com.kshrd.moviesservice.repository;

import com.kshrd.moviesservice.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie,Long> {

}
