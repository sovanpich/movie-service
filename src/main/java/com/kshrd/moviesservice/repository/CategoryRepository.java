package com.kshrd.moviesservice.repository;

import com.kshrd.moviesservice.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Long> {

    Category findCategoryById (Long id);

}
