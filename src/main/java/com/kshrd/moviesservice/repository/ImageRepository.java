package com.kshrd.moviesservice.repository;

import com.kshrd.moviesservice.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image,Long> {

    Image findImageById(Long id);
}
